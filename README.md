# Что это?

Шаблон статьи для Сборника Трудов НИИСИ РАН.

Скачать можно или с помощью `git` или по [этой ссылке](https://bitbucket.org/Ykuz61/trudy_niisi_template/downloads/) и далее **Download repository** (или **Скачать репозиторий**). 

# Библиография
Библиография формируется с помощью [biblatex-biber](https://github.com/plk/biber). Подробнее о нём [на русском](https://riptutorial.com/ru/latex/example/12683/%D0%BE%D1%81%D0%BD%D0%BE%D0%B2%D0%BD%D0%B0%D1%8F-%D0%B1%D0%B8%D0%B1%D0%BB%D0%B8%D0%BE%D0%B3%D1%80%D0%B0%D1%84%D0%B8%D1%8F-%D1%81-%D0%B1%D0%B8%D0%B1%D0%B5%D1%80%D0%BE%D0%BC) и [на английском](https://en.wikibooks.org/wiki/LaTeX/Bibliographies_with_biblatex_and_biber#Setting_up_biber).

Чтобы скомпилировать штатными средствами, вам нужно будет последовательно запустить 3 команды:

1. `pdflatex` для создания вспомогательного файла, который сообщает biber, какие источники нужны.
2. `biber` для создания вспомогательного файла со всеми источниками, которые могут использоваться `pdflatex`.
3. `pdflatex` включить вспомогательный файл и создать PDF-файл.

# Автоматизация сборки

Для упрощения рекомендуется использоваться вспомогательными системами сборки документа Latex:

  [latexmk](http://users.phys.psu.edu/~collins/software/latexmk-jcc/) ([документация](https://mg.readthedocs.io/latexmk.html)),
  [latexrun](https://github.com/aclements/latexrun),
  [tectonic](https://tectonic-typesetting.github.io), или
  [arara](https://github.com/cereda/arara).

Вышеуказанные инструменты позволяют автоматизировать ряд процессов: сборку с библиографией, скачивание недостающих пакетов и т.п..

# Рекомендации по написанию текстов в Latex

Прежде чем приступать к написанию статей, рекомендуем ознакомиться с различными рекомендациями. Следование им позволит более эффективно и аккуратно использовать инструментарий Latex, а также избежать проблем со сборкой вашего текста в различных системах. Кроме того, аккуратно подготовленный документ облегчает дальнейшую вёрстку и редактирование, в т.ч. силами редакции.

- [Набор и вёрстка в системе LaTeX (С. М. Львовский)](https://www.mccme.ru/free-books/llang/newllang.pdf), рус.
- [Essential Guide to LaTeX2ε Usage (l2tabu)](http://mirror.macomnet.net/pub/CTAN/info/l2tabu/english/l2tabuen.pdf), англ.
- [Short Math Guide for LaTeX (American Mathematical Society)](ftp://ftp.ams.org/pub/tex/doc/amsmath/short-math-guide.pdf), англ.
- [Введение в LaTeX для пользователей Windows](http://xgu.ru/wiki/%D0%92%D0%B2%D0%B5%D0%B4%D0%B5%D0%BD%D0%B8%D0%B5_%D0%B2_LaTeX_%D0%B4%D0%BB%D1%8F_%D0%BF%D0%BE%D0%BB%D1%8C%D0%B7%D0%BE%D0%B2%D0%B0%D1%82%D0%B5%D0%BB%D0%B5%D0%B9_Windows), рус.
